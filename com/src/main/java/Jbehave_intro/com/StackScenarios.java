package Jbehave_intro.com;
import java.util.List;

import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class StackScenarios extends JUnitStories{
	public StackScenarios() {
		super();
	}
 
	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new StackSteps());
	}
 
	@Override
	protected List<String> storyPaths() {

		return new StoryFinder().findPaths
				(org.jbehave.core.io.CodeLocations.codeLocationFromPath
						("src/main/resouces"), "**/StackStories.story", "");
	}
}























